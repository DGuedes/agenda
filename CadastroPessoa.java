import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;
    String entradaOpcao;
    String umaOpcao;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();
    
    //interagindo com usuário
    do{
    System.out.println("Digite o nome da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umNome = entradaTeclado;
    umaPessoa.setNome(umNome);

    System.out.println("Digite o telefone da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umTelefone = entradaTeclado;
    umaPessoa.setTelefone(umTelefone);

    System.out.println("Digite a idade da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umaIdade = entradaTeclado;
    umaPessoa.setIdade(umaIdade);

    System.out.println("Digite o sexo da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umSexo = entradaTeclado;
    umaPessoa.setSexo(umSexo);

    System.out.println("Digite o RG da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umRG = entradaTeclado;
    umaPessoa.setRG(umRG);

    System.out.println("Digite o CPF da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umCPF = entradaTeclado;
    umaPessoa.setCPF(umCPF);

    System.out.println("Digite o Endereço da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umEndereco = entradaTeclado;
    umaPessoa.setEndereco(umEndereco);

    System.out.println("Hangout:");
    entradaTeclado = leitorEntrada.readLine();
    String umHangout = entradaTeclado;
    umaPessoa.setHangout(umHangout);

    System.out.println("E-mail da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umEmail = entradaTeclado;
    umaPessoa.setEmail(umEmail);
	
    //adicionando uma pessoa na lista de pessoas do sistema
    String mensagem = umControle.adicionar(umaPessoa);

    //conferindo saída
    System.out.println("=================================");
    System.out.println(mensagem);
    System.out.println("=)");
    System.out.println("Pessoa adicionada com sucesso. Para adicionar outra pessoa, entre com qualquer valor diferente de 1.");
    entradaOpcao = leitorEntrada.readLine();
    umaOpcao = entradaOpcao;
    String umaLogica = "1";
    }while(!umaOpcao.equals("1"));
   
    System.out.println("Programa encerrado.");
  }
}
